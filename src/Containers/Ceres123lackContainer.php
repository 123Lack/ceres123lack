<?php

namespace Ceres123lack\Containers;

use Plenty\Plugin\Templates\Twig;

class Ceres123lackContainer
{
    public function call(Twig $twig):string
    {
        return $twig->render('Ceres123lack::content.Ceres123lack');
    }
}